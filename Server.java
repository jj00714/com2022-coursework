package jj00714_protocol;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
  private static final int PORT = 9090;

  static LinkedList<ClientHandler> clients = new LinkedList<>();
  private static ExecutorService pool = Executors.newFixedThreadPool(10);
  static HashMap<String, ClientHandler> uuids = new HashMap<>();

  public static void main(String[] args) throws IOException {
    ServerSocket listener = new ServerSocket(PORT);

    while (true) {
      System.out.println("[SERVER] Waiting for client connection...");
      Socket client = listener.accept();
      System.out.println("[SERVER] Connected to client!");
      ClientHandler clientThread = new ClientHandler(client);

      pool.execute(clientThread);
    }

  }

}
