package jj00714_protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
  private static final String SERVER_IP = "127.0.0.1";
  private static final int SERVER_PORT = 9090;

  public static void main(String[] args) throws IOException {
    Socket socket = new Socket(SERVER_IP, SERVER_PORT);

    BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));

    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

    while (true) {
      System.out.print("> ");
      String command = keyboard.readLine();
      command = command.toUpperCase();
      out.println(command);

      while (true) {
        if (command.equals("")) {
          break;
        }

        String serverResponse = input.readLine();
        if (serverResponse.equals("")) {
          break;
        }

        System.out.println("[SERVER] " + serverResponse);

        if (serverResponse.equals("GOODBYE")) {
          socket.close();
        }
      }

    }
  }
}
