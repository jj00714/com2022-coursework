package jj00714_protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;

public class ClientHandler implements Runnable {
  private Socket client;
  private BufferedReader in;
  private PrintWriter out;
  private UUID uuid;
  private ClientHandler initialClient = null;

  public ClientHandler(Socket clientSocket) throws IOException {
    this.client = clientSocket;
    in = new BufferedReader(new InputStreamReader(client.getInputStream()));
    out = new PrintWriter(client.getOutputStream(), true);
  }

  public UUID getUUID() {
    return this.uuid;
  }

  public ClientHandler getClientHandler() {
    if (this.initialClient == null) {
      return this;
    } else {
      return this.initialClient;
    }
  }

  @Override
  public void run() {
    try {
      while (true) {
        String request = in.readLine();

        if (request.contains("POSITION")) {
          out.println("POSITION " + (Server.clients.indexOf(getClientHandler()) + 1));
          out.println("");
        }

        else if (request.contains("HELLO")) {
          Server.clients.add(this);
          out.println("WELCOME");
          this.uuid = getUUID().randomUUID();
          Server.uuids.put(this.uuid.toString(), this);
          out.println("UUID " + uuid);
          out.println("POSITION " + (Server.clients.indexOf(this) + 1));
          if (Server.clients.indexOf(this) == 0) {
            out.println("NEXT! Type \"DONE\" when complete.");
          }
          out.println("");
        }

        else if (request.contains("UUID")) {
          String[] msg = request.split(" ");
          if (Server.uuids.containsKey(msg[1].toLowerCase())) {
            this.initialClient = Server.uuids.get(msg[1].toLowerCase());
            out.println("WELCOME_BACK");
            out.println("POSITION " + (Server.clients.indexOf(this.initialClient) + 1));
            if ((Server.clients.indexOf(this.initialClient) + 1) == 1) {
              out.println("NEXT! Type \"DONE\" when complete.");
            }
            out.println("");
          } else {
            out.println("INCORRECT_UUID");
            out.println("Disconnected from Server");
            out.println("");
            break;
          }
        }

        else if (request.contains("DONE")) {
          Server.clients.remove(this);
          out.println("Disconnected from Server");
          out.println("");
        }

        else if (request.contains("POSITION")) {
          out.println("POSITION " + (Server.clients.indexOf(this) + 1));
          out.println("");
        }

        else if (request.equals("GOODBYE")) {
          out.println("GOODBYE");
          out.println("Disconnected from Server");
          Server.uuids.remove(getClientHandler().getUUID().toString());
          out.println("");
          Server.clients.remove(getClientHandler());
        } else {
          out.println(
              "UNKNOWN_COMMAND: Request is unrecognised. Please say \"HELLO\", \"POSITION\", \"UUID\", \"GOODBYE\", or \"DONE\"");
          out.println("");
        }
      }
    } catch (IOException e) {
      System.err.println("IO exception in client handler");
      System.err.println(e.getStackTrace());
    } finally {
      out.close();
      System.out.println("[SERVER] Disconnected with client!");
      try {
        in.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
